/*

    Copyright 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022  joshua.tee@gmail.com

    This file is part of wX.

    wX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    wX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with wX.  If not, see <http://www.gnu.org/licenses/>.

*/

package joshuatee.wx.radar

import android.content.Context
import joshuatee.wx.objects.FutureVoid
import joshuatee.wx.objects.ObjectPolygonWarning
import joshuatee.wx.objects.ObjectPolygonWatch
import joshuatee.wx.objects.PolygonType
import joshuatee.wx.settings.RadarPreferences

object NexradLayerDownload {

    fun download(
            context: Context,
            numberOfPanes: Int,
            archiveMode: Boolean,
            wxglRender: WXGLRender,
            wxglSurfaceView: WXGLSurfaceView,
            wxglTextObjects: List<WXGLTextObject>,
            radarUpdateFn: () -> Unit,
            showWpcFronts: Boolean = true
    ) {
        //
        // Warnings
        //
        ObjectPolygonWarning.polygonDataByType.values.forEach {
            if (it.isEnabled) {
                FutureVoid(context, it::download) {
                    if (!wxglRender.product.startsWith("2")) {
                        UtilityRadarUI.plotWarningPolygon(it.type, wxglSurfaceView, wxglRender)
                    }
                    radarUpdateFn()
                }
            }
        }
        //
        // MCD / Watch download/update
        //
        if (PolygonType.MCD.pref && !archiveMode) {
            FutureVoid(context, {
                ObjectPolygonWatch.polygonDataByType[PolygonType.WATCH]!!.download(context)
            }) {
                if (!wxglRender.product.startsWith("2")) {
                    UtilityRadarUI.plotPolygons(PolygonType.WATCH, wxglSurfaceView, wxglRender, archiveMode)
                }
            }
            FutureVoid(context, {
                ObjectPolygonWatch.polygonDataByType[PolygonType.MCD]!!.download(context)
            }) {
                if (!wxglRender.product.startsWith("2")) {
                    UtilityRadarUI.plotPolygons(PolygonType.MCD, wxglSurfaceView, wxglRender, archiveMode)
                }
            }
        }
        //
        // MPD download/update
        //
        if (PolygonType.MPD.pref && !archiveMode) {
            FutureVoid(context, { ObjectPolygonWatch.polygonDataByType[PolygonType.MPD]!!.download(context) }, {
                if (!wxglRender.product.startsWith("2")) {
                    UtilityRadarUI.plotPolygons(PolygonType.MPD, wxglSurfaceView, wxglRender, archiveMode)
                }
            })
        }
        //
        // WPC Fronts download/update
        //
        if (showWpcFronts) {
            if (RadarPreferences.radarShowWpcFronts && !archiveMode) {
                FutureVoid(context, { UtilityWpcFronts.get(context) }, {
                    if (!wxglRender.product.startsWith("2")) {
                        UtilityRadarUI.plotWpcFronts(wxglSurfaceView, wxglRender, archiveMode)
                    }
                    UtilityWXGLTextObject.updateWpcFronts(numberOfPanes, wxglTextObjects)
                })
            }
        }
    }
}
